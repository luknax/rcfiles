(require 'package)

(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

;; list of must-have packages
(setq package-list '(ace-jump-mode
		     auto-complete
		     avy
		     command-log-mode
		     eglot
		     elisp-benchmarks
		     elisp-slime-nav
		     evil
		     evil-leader
		     evil-mc
		     evil-textobj-syntax
		     evil-visualstar
		     flycheck
		     gcmh
		     ggtags
		     git-gutter
		     golden-ratio-scroll-screen
		     helm
		     helm-gtags
		     helm-projectile
		     highlight-numbers
		     hl-todo
		     lsp-mode
		     magit
		     markdown-mode
		     names
		     native-complete
		     paredit
		     pdf-tools
		     projectile
		     quelpa
		     rmsbolt
		     slime
		     transpose-frame
		     undo-tree
		     vdiff
		     veri-kompass
		     wgrep
		     yaml-mode))

(package-initialize)

;; check and install packages
(defvar pkg-refreshed nil)
(with-demoted-errors
    (when (file-exists-p package-user-dir)
      (dolist (package package-list)
	(unless (package-installed-p package)
	  (unless pkg-refreshed
	    (package-refresh-contents)
	    (setq pkg-refreshed t))
	  (package-install package)))))

(defun ping-reachable-p (host)
  (= 0 (call-process "ping" nil nil nil "-c" "1" "-W" "1" host)))

;; load local path
(when (file-exists-p "~/.emacs.d/lisp/")
  (add-to-list 'load-path "~/.emacs.d/lisp/"))

;; enable and customize garbage collection
(gcmh-mode 1)
(setq gcmh-verbose nil)
(setq gcmh-time-constant (* float-pi float-pi))
(setq gcmh-high-cons-threshold #x20000000)

;; disable graphic features
;; - startup message
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)
;; - menu bar
(menu-bar-mode -1)
;; - toolbar
(tool-bar-mode -1)
;; - scroll bars
(scroll-bar-mode -1)
;; - use visible bell
(setq visible-bell t)

;; auto revert mode
(global-auto-revert-mode 1)
;; enable mouse in xterm
(xterm-mouse-mode 1)
;; move cursor to help window when opened
(setq help-window-select t)
;; confirm before closing
(setq confirm-kill-emacs 'y-or-n-p)
;; ask y/n instead of yes/no
(advice-add 'yes-or-no-p :override #'y-or-n-p)
;; do not pop up dialog boxes even if running with gui
(setq use-dialog-box nil)
;; enable narrow-to-region
(put 'narrow-to-region 'disabled nil)
;; do not create backup/autosave files
(setq make-backup-files nil)
(setq auto-save-default nil)
;; enable list-threads
(put 'list-threads 'disabled nil)
;; highlight matching parenthesis
(show-paren-mode 1)
(setq show-paren-delay 0)
;; save/restore minibuffer history
(setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))
(setq savehist-file "~/.emacs.d/savehist")
(savehist-mode 1)

;; scroll options
;; - mouse scroll 2 lines at a time
(setq mouse-wheel-scroll-amount '(2 ((shift) . 1)))
;; - keyboard scroll one line at a time
(setq scroll-step 1)
;; - don't accelerate mouse scrolling
(setq mouse-wheel-progressive-speed nil)
;; - scroll window under mouse
(setq mouse-wheel-follow-mouse 't)
;; - redraw immediately when scrolling
(setq fast-but-imprecise-scrolling nil)
(setq jit-lock-defer-time 0)
;; - scroll less than full page using page-up/down
(require 'golden-ratio-scroll-screen)
(setq golden-ratio-scroll-highlight-flag nil)
(global-set-key [next] #'golden-ratio-scroll-screen-up)
(global-set-key [prior] #'golden-ratio-scroll-screen-down)

;; window options
;; - move between windows
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))
;; avoid conflicts between windmove and org
(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)
;; - resize windows
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)
;; - move in window configuration history
(when (fboundp 'winner-mode)
  (winner-mode 1))
;; - rotate/flip splits in current frame
(when (fboundp 'transpose-frame)
  (global-set-key (kbd "C-c C-<up>") 'flip-frame)
  (global-set-key (kbd "C-c C-<left>") 'flop-frame)
  (global-set-key (kbd "C-c C-<right>") 'rotate-frame-clockwise))
;; minimum window height to be split horizontally
(setq split-height-threshold 60)

;; org-mode
(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (python . t)))

;; evil mode
;; set variables needed before loading evil
;; - use fine grain undo
(setq evil-want-fine-undo t)
;; - Y yanks to end of line
(setq evil-want-Y-yank-to-eol t)
(require 'evil)
(require 'evil-textobj-syntax)
(evil-mode t)
;; undo system
(evil-set-undo-system 'undo-redo)
;; movements take into account visual line
(define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
(define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
;; allow horizontal movement to cross lines
(setq-default evil-cross-lines t)
;; jump commands act only on current buffer
(setq evil-jumps-cross-buffers nil)
(setq evil-jumps-max-length 500)
;; use default vim search
(evil-select-search-module 'evil-search-module 'evil-search)
;; search is case sensitive
(setq case-fold-search nil)
(setq evil-ex-search-case nil)
;; allow to search visual selection
(global-evil-visualstar-mode)
;; use faces instead of overlays for search highlight
;; as they properly handle editing and region select
(setq-default font-lock-keywords-case-fold-search nil)
(defun evil-ex-hl-active-overlay-to-face (&rest args)
  (when (evil-ex-hl-active-p 'evil-ex-search)
    (unhighlight-last)
    (evil-ex-delete-hl 'evil-ex-search)
    (highlight-regexp (evil-ex-pattern-regex evil-ex-search-pattern) 'lazy-highlight)))
(advice-add 'evil-ex-start-search :after #'evil-ex-hl-active-overlay-to-face)
(advice-add 'evil-ex-search :after #'evil-ex-hl-active-overlay-to-face)
;; spacebar removes highlight
(defun unhighlight-last ()
  (interactive)
  (when (and (boundp 'hi-lock-interactive-patterns)
	     (equal (cadr (cadr (cadar hi-lock-interactive-patterns))) 'lazy-highlight))
    (unhighlight-regexp (caar hi-lock-interactive-patterns))))
(define-key evil-normal-state-map (kbd "SPC") 'unhighlight-last)
;; setup leader
(setq evil-leader/in-all-states 1)
(global-evil-leader-mode)
(evil-leader/set-leader ",")
;; leader mappings:
;; - save buffer
(evil-leader/set-key "," 'evil-write)
;; - close window
(evil-leader/set-key "." 'delete-window)
;; - toggle underscore syntax meaning
(defun toggle-syntax (char)
  (if (char-equal (char-syntax char) ?_)
      (modify-syntax-entry char "w")
    (modify-syntax-entry char "_")))
(defun toggle-underscore-syntax ()
  (interactive)
  (toggle-syntax ?_))
(defun toggle-hypen-syntax ()
  (interactive)
  (toggle-syntax ?-))
(evil-leader/set-key "_" 'toggle-underscore-syntax)
(evil-leader/set-key "-" 'toggle-hypen-syntax)
;; - avy/ace-jump mappings
(evil-leader/set-key "/" 'evil-ace-jump-char-mode) ; evil-avy-goto-char
(evil-leader/set-key "c" 'evil-ace-jump-char-mode) ; evil-avy-goto-char
(evil-leader/set-key "w" 'evil-ace-jump-word-mode) ; evil-avy-goto-subword-1
(evil-leader/set-key "l" 'evil-ace-jump-line-mode) ; evil-avy-goto-line

;; ido-mode
(require 'ido)
(require 'ido-other-window)
(setq ido-default-file-method 'maybe-frame)

;; projectile
(require 'projectile)
(projectile-global-mode)
(setq projectile-enable-caching t)

;; native-complete
(with-eval-after-load 'shell
  (native-complete-setup-bash))

;; wgrep
(require 'wgrep)

;; paredit
(require 'paredit)

;; helm
(helm-mode 1)
(helm-adaptive-mode)
(setq history-delete-duplicates t)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x C-d") 'helm-projectile)
(global-set-key (kbd "C-M-z") 'helm-resume)
(define-key helm-map (kbd "C-z")  #'helm-select-action)
(define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
(define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
(define-key helm-map (kbd "<backtab>") #'helm-find-files-up-one-level)
(setq helm-input-idle-delay 0.1)
(setq helm-buffer-max-length 50)
(setq helm-always-two-windows nil)
(setq helm-display-buffer-default-height 20)
(setq helm-default-display-buffer-functions '(display-buffer-in-side-window))
(setq helm-show-action-window-other-window nil)

;; dired
(setq dired-listing-switches "-alh")
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; magit
(require 'magit-mode)
(global-set-key (kbd "C-x g") 'magit-status)
(setq magit-log-section-commit-count 25)
;; - invalidate projectile cache when changing branch
(defun magit-invalidate-cache (&rest args)
  (projectile-invalidate-cache nil))
(advice-add 'magit-checkout :after #'magit-invalidate-cache)

;; git-gutter
(require 'git-gutter)
(global-git-gutter-mode +1)
(global-set-key (kbd "C-x p") 'git-gutter:previous-hunk)
(global-set-key (kbd "C-x n") 'git-gutter:next-hunk)

;; quelpa
(setq quelpa-checkout-melpa-p nil)
(setq quelpa-update-melpa-p nil)
(setq quelpa-self-upgrade-p nil)
(require 'quelpa)

;; flycheck
(require 'flycheck)

;; auto-complete
(ac-config-default)

;; elisp-slime-nav
;; - M-. runs the command elisp-slime-nav-find-elisp-thing-at-point
;; - M-, to navigate back
(require 'elisp-slime-nav)
(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook 'turn-on-elisp-slime-nav-mode))
;; make it coexist with evil
(define-key evil-normal-state-map (kbd "M-.")
  `(menu-item "" evil-repeat-pop :filter
	      ,(lambda (cmd) (if (eq last-command 'evil-repeat-pop) cmd))))

;; ibuffer
(require 'ibuffer)
;; - make default buffer listing
(defalias 'list-buffers 'ibuffer)
;; - auto-refresh
(add-hook 'ibuffer-mode-hook (lambda () (ibuffer-auto-mode 1)))
;; - size filed format
(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
   ((> (buffer-size) 1000000000) (format "%7.1fG" (/ (buffer-size) 1000000000.0)))
   ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
   ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
   (t (format "%8d" (buffer-size)))))
;; - customize column size
(setq ibuffer-formats
      '((mark modified read-only " "
	      (name 80 80 :left :nil) " "
	      (size-h 9 -1 :right) " "
	      (mode 16 16 :left :elide) " "
	      filename-and-process)))
;; - group buffers by type
(setq ibuffer-saved-filter-groups
      (quote (("default"
	       ("shell" (or
			 (mode . shell-mode)
			 (mode . term-mode)
			 (mode . eshell-mode)
			 (name . "^\\*Shell Command Output\\*$")))
	       ("midas" (mode . smime))
	       ("app" (name . ".*\\.app"))
	       ("yaml" (mode . yaml-mode))
	       ("python" (or
			  (mode . python-mode)
			  (mode . inferior-python-mode)
			  (name . "^\\*Python \\(Check\\|Doc\\)\\*$")))
	       ("verilog " (mode . verilog-mode))
	       ("C" (or
		     (derived-mode . c-mode)
		     (mode . c++-mode)))
	       ("text" (mode . text-mode))
	       ("asm" (mode . asm-mode))
	       ("sh" (or
			 (mode . sh-mode)
			 (mode . conf-unix-mode)))
	       ("emacs" (or
			 (mode . emacs-lisp-mode)
			 (mode . lisp-interaction-mode)
			 (mode . help-mode)
			 (mode . Info-mode)
			 (mode . package-menu-mode)
			 (mode . finder-mode)
			 (mode . Custom-mode)
			 (mode . apropos-mode)
			 (mode . ioccur-mode)
			 (mode . occur-mode)
			 (mode . reb-mode)
			 (mode . calc-mode)
			 (mode . calc-trail-mode)
			 (mode . messages-buffer-mode)))
	       ("lisp" (or
			(mode . lisp-mode)
			(mode . slime-repl-mode)
			(mode . slime-inspector-mode)
			(name . "^\\*slime-\\(description\\|compilation\\|xref\\)\\*$")
			(name . "^\\*sldb .*\\*$")
			(filename . "^/usr/local/doc/HyperSpec/")))
	       ("LaTeX" (or
			 (mode . latex-mode)
			 (mode . tex-shell)
			 (mode . TeX-output-mode)
			 (name . "^\\*\\(Latex Preview Pane \\(Welcome\\|Errors\\)\\|pdflatex-buffer\\)\\*$")))
	       ("pdf" (or
		       (mode . doc-view-mode)
		       (mode . pdf-view-mode)))
	       ("org" (or
		       (derived-mode . org-mode)
		       (mode . org-agenda-mode)
		       (filename . "OrgMode")))
	       ("planner" (or
			   (name . "^\\*Calendar\\*$")
			   (name . "^diary$")
			   (mode . muse-mode)))
	       ("git" (or
		       (derived-mode . magit-mode)
		       (filename . "\\.git\\(ignore\\|attributes\\)$")))
	       ("diff" (or
			(mode . diff-mode)
			(mode . ediff-mode)
			(name . "^\\*[Ee]?[Dd]iff.*\\*$")))
	       ("dired" (or
			 (mode . dired-mode)
			 (mode . wdired-mode)
			 (mode . archive-mode)
			 (mode . proced-mode)))
	       ("man" (or
		       (mode . Man-mode)
		       (mode . woman-mode)))
	       ("data" (or
			(filename . ".*\\.\\([ct]sv\\|dat\\)$")))
	       ("misc" (name . "^\\*.+\\*$"))))))
(add-hook 'ibuffer-mode-hook
	  (lambda () (ibuffer-switch-to-saved-filter-groups "default")))
(setq ibuffer-show-empty-filter-groups nil)
(setq ibuffer-jump-offer-only-visible-buffers t)

;; font
(add-to-list 'default-frame-alist
             '(font . "DejaVu Sans Mono:pixelsize=14"))
;; color theme
(add-to-list 'custom-theme-load-path
	     (file-name-as-directory "~/.emacs.d/themes/"))
(load-theme 'oblivion t t)
(enable-theme 'oblivion)
;; do not set the background when opened in terminal
(defun reset-background (frame)
  (unless (display-graphic-p frame)
    (set-face-background 'default "black" frame)))
(defun reset-background-selected-frame ()
  (interactive)
  (reset-background (selected-frame)))
(add-hook 'after-make-frame-functions 'reset-background)
(add-hook 'window-setup-hook 'reset-background-selected-frame)

;; highlight TODOs
(require 'hl-todo)
(setq hl-todo-keyword-faces
      '(("TODO" . "#555753")))
(global-hl-todo-mode)

;; pdf-tools
(pdf-tools-install)
(setq doc-view-cache-directory "~/doc-view-cache")
;; when pdf is visited it is converted and cached in `doc-view-cache-directory`,
;; the corresponding text representation keeps page boundaries
(defun text-to-pdf-jump ()
  (interactive)
  (let ((page-str (what-page))
	(pdf-name (doc-view-cache-txt-to-pdf-file (buffer-file-name))))
    (if (get-buffer pdf-name)
	(switch-to-buffer pdf-name)
      (error "PDF buffer not found: %s" pdf-name))
    (pdf-view-goto-page (string-to-number (substring page-str (string-match-p "[0-9]" page-str))))))
(define-key text-mode-map (kbd "C-c j") 'text-to-pdf-jump)

(defun doc-view-cache-txt-to-pdf-file (file-name)
  (let ((dir-name (file-name-directory file-name))
	(pdf-name))
    (unless dir-name
      (error "Not a file: %s" file-name))
    (unless (string-match "\\([^/]+\\.pdf\\)" dir-name)
      (error "Cannot find PDF file linked to %s" file-name))
    (match-string 1 dir-name)))

;; remove trailing spaces before saving
(defun safe-delete-trailing-whitespace ()
  (when (derived-mode-p 'prog-mode)
    (delete-trailing-whitespace)))
(add-hook 'before-save-hook 'safe-delete-trailing-whitespace)

;; customize programming mode:
(add-hook 'prog-mode-hook 'highlight-numbers-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'column-number-mode)
(add-hook 'prog-mode-hook (lambda () (setq truncate-lines t)))

;; version control
(require 'vc)
(defvar vc-git-grep-at-point-use-root nil)
(defvar vc-git-grep-at-point-dir nil)
(defun vc-git-grep-at-point ()
  "Call vc-git-grep on word-at-point"
  (interactive)
  (let ((name (buffer-file-name (current-buffer)))
	(dir vc-git-grep-at-point-dir))
    (if vc-git-grep-at-point-use-root
	(setq dir (vc-find-root name ".git"))
      (progn
	(unless (and (stringp dir) (string-prefix-p dir name))
	  (setq dir (file-name-directory name)))
	(setq dir (read-directory-name "Run vc-git-grep in directory: " dir dir t))
	(setq vc-git-grep-at-point-dir dir)))
      (vc-git-grep (regexp-quote (word-at-point)) "" dir)))
(global-set-key (kbd "C-c v") #'vc-git-grep-at-point)

;; helm-gtags
(require 'helm-gtags)
(setq helm-gtags-ignore-case t)
(setq helm-gtags-auto-update t)
(setq helm-gtags-use-input-at-cursor t)
;; helm-gtags keymap
(define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
(define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
(define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
(define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
(define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
(define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history)

;; ggtags
(require 'ggtags)
(add-hook 'c-mode-common-hook 'ggtags-mode)
(add-hook 'asm-mode-hook 'ggtags-mode)
(add-to-list 'display-buffer-alist
	     '("*ggtags-global*"
	       (display-buffer-in-side-window)
	       (inhibit-same-window . t)
	       (window-height . 0.4)))

;; ctags
(defun create-ctags ()
  "Create tags file."
  (interactive)
  (let ((default-directory
	  (read-directory-name "Create tags for directory: ")))
    (shell-command "~/ctags/ctags -e -R .")
    (visit-tags-table "./")))

;; never insert '\t' but for lisp
(setq-default indent-tabs-mode nil)
(defun allow-tabs ()
  (setq-local indent-tabs-mode t))
(dolist (hook '(emacs-lisp-mode-hook lisp-mode-hook lisp-interaction-mode-hook))
  (add-hook hook 'allow-tabs))

;; function to limit TAB power
(defun bridle-tabs ()
  (interactive)
  ;; tabulation is two spaces
  (setq-local tab-width 2)
  ;; tab key inserts tabulation
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (local-set-key (kbd "<tab>") 'tab-to-tab-stop)
  ;; indentation is done with C-tab
  (local-set-key (kbd "<C-tab>") 'indent-for-tab-command))

;; Verilog customizations
;; - newline indents relative
(defun ret-indent-relative ()
  (interactive)
  (newline)
  (indent-relative t))
;; - line indentation is relative
(defun remap-indent-line ()
  (interactive)
  (setq-local indent-line-function 'indent-relative))
;; - cusomize syntax highlighting
(defun verilog-custom-syntax ()
  (interactive)
  (font-lock-add-keywords nil
			  ;; all capitalized words are preprocessor macros
			  '(("\\<[A-Z_][A-Z0-9_]*\\>" . 'font-lock-preprocessor-face)
			    ;; correctly handle Verilog numbers
			    ("'[hdb]?[0-9a-fA-FzxZX_]+\\>" . 'font-lock-constant-face)
			    ("\\<[0-9]+\\>" . 'font-lock-constant-face))))
(defun verilog-mode-customizations ()
  ;; set all indentations to two spaces
  (setq verilog-indent-level 2)
  (setq verilog-indent-level-module 2)
  (setq verilog-indent-level-declaration 2)
  (setq verilog-indent-level-behavioral 2)
  (setq verilog-indent-level-directive 2)
  (setq verilog-cexp-indent 2)
  (setq verilog-case-indent 2)
  ;; disable automatic indentation
  (setq verilog-auto-indent-on-newline t)
  (setq verilog-tab-always-indent nil)
  (setq verilog-indent-begin-after-if nil)
  (setq verilog-auto-newline nil)
  (setq verilog-auto-end-comments nil)
  (setq verilog-auto-lineup nil)
  ;; change line indentation function
  (add-hook 'verilog-mode-hook 'remap-indent-line)
  ;; disable abbreviations
  (abbrev-table-put verilog-mode-abbrev-table :enable-function (lambda () nil))
  ;; configure indentation for some keys
  (define-key verilog-mode-map (kbd ";") 'self-insert-command)
  (define-key verilog-mode-map (kbd "RET") 'ret-indent-relative)
  ;; change tab behavior
  (add-hook 'verilog-mode-hook 'bridle-tabs)
  ;; custom syntax
  (add-hook 'verilog-mode-hook 'verilog-custom-syntax))
(eval-after-load 'verilog-mode '(verilog-mode-customizations))

(defun smime-customizations ()
  (setq smime-double-quotes-are-quotes nil)
  (setq smime-syntax-table (smime-syntax-table-gen verilog-mode-syntax-table))
  (abbrev-table-put smime-abbrev-table :enable-function (lambda () nil)))

(defun smime-add-render-dir (directory)
  "Select a rendered directory to be added in smime-render-dirs"
  (interactive "DMidas rendered dir: ")
  (setq smime-render-dirs
	(cons directory (delete directory smime-render-dirs))))

;; load smime
(when (file-exists-p "~/midas/etc/SMIME/")
  (add-to-list 'load-path "~/midas/etc/SMIME/")
  (require 'smime)
  (eval-after-load 'smime '(smime-customizations)))
(add-to-list 'auto-mode-alist '("\\.hdg" . smime))
;; load tarmac-mode
(when (ping-reachable-p "eu-gerrit-2.euhpc.arm.com")
  (quelpa '(tarmac-mode
	    :fetcher git
	    :url "https://eu-gerrit-2.euhpc.arm.com/emacs-rtl/tarmac-mode.git"
	    :files (:defaults "*.xpm" "db/*"))
	  :upgrade t)
  (require 'tarmac-mode))

;; Shell customizations
;; - do not buffer data read from other processes (bug#44007)
(setq process-adaptive-read-buffering nil)
;; - clean shell and reset compilation mode
(defun shell-clean ()
  (interactive)
  (let ((smime-shell (and (boundp 'smime-compilation-shell-minor-mode)
			  smime-compilation-shell-minor-mode)))
    (delete-region (point-min) (point-at-bol))
    (fundamental-mode)
    (shell-mode)
    (if smime-shell
	(smime-compilation-shell-minor-mode)
      (compilation-shell-minor-mode))))
;; - customize error regexp for TB
(add-to-list 'compilation-error-regexp-alist-alist
             '(tb-error-1
               "^ *| *File \"\\([^\"]*\\)\", line \\([0-9]+\\)"
               1 2))
(push 'tb-error-1 compilation-error-regexp-alist)
(add-to-list 'compilation-error-regexp-alist-alist
             '(tb-error-2
               "^ *[|]* *\\*\\* Error.*: \\(([^ ]+)\\| *\\) *\\([^ ]+\\)(\\([0-9]+\\))"
               1 2))
(push 'tb-error-2 compilation-error-regexp-alist)
(add-to-list 'compilation-error-regexp-alist-alist
             '(tb-error-3
               "Error-\\[[A-Z0-9_-]+\\] .*\n *|? *\\([^ ]+\\), \\([0-9]+\\)"
               1 2))
(push 'tb-error-3 compilation-error-regexp-alist)
(add-to-list 'compilation-error-regexp-alist-alist
             '(tb-error-4
	       "Error-\\[[A-Z0-9_-]+\\] .*\n *|? *.*\n *|? *\\[\\([^ ]+\\):\\([0-9]+\\)\\]"
               1 2))
(push 'tb-error-4 compilation-error-regexp-alist)
(add-to-list 'compilation-error-regexp-alist-alist
             '(tb-error-5
	       "\\(halcheck\\|halsynth\\|xmelab\\): *\[A-Z0-9_*,-]+ *\\([^ ]+\\),\\([0-9]+\\)"
               2 3))
(push 'tb-error-5 compilation-error-regexp-alist)

;; save custom in a dedicated file and source it
(setq custom-file "~/.emacs.d/custom.el")
(write-region "" nil custom-file 'append)
(load custom-file)
